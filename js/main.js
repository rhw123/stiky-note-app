window.addEventListener("DOMContentLoaded", (event) => {
    event.preventDefault();

    ambilNotes();
})


const notesContainer = document.getElementById("app");
const tambahnotes = document.querySelector(".tambah_node");

ambilNotes().forEach(note => {
    const noteElement = buatNoteBaru(note.id, note.content);
    notesContainer.insertBefore(noteElement, tambahnotes);
});

tambahnotes.addEventListener("click", () => tambahNotes());

function ambilNotes() {
    return JSON.parse(localStorage.getItem("stickynotes-notes") || "[]");
}

function simpanNotes(notes) {
    localStorage.setItem("stickynotes-notes", JSON.stringify(notes));
}

function buatNoteBaru(id ,content) {
    const element = document.createElement("textarea");
    element.classList.add("note");
    element.value = content;
    element.placeholder = "Stiky Note Anda Kosong";
    element.addEventListener("change", function() {
        updateNotes(id, element.value); 
    });

    element.addEventListener("dblclick", function() {
        const Ykin = confirm("Apakah Kamu yakin ingin menghapus stiky notes ini ? ");
        if (Ykin) {
            hapusNotes(id, element);
        }
    })
    return element;
}

function tambahNotes() {
    const notes = ambilNotes();
    const noteobject = {
        id: Math.floor(Math.random() * 100000),
        content: ""
    };

    const noteElement = buatNoteBaru(noteobject.id, noteobject.content);
    notesContainer.insertBefore(noteElement, tambahnotes);
    notes.push(noteobject);
    simpanNotes(notes);
}

function updateNotes(id , item_baru) {
    const notes = ambilNotes();
    const targetnotes = notes.filter(note => note.id == id)[0];

    targetnotes.content = item_baru;
    simpanNotes(notes);
}

function hapusNotes(id ,element) {
    const notes = ambilNotes().filter(note => note.id != id);
    simpanNotes(notes);
    notesContainer.removeChild(element);
}